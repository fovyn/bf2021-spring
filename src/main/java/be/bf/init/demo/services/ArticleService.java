package be.bf.init.demo.services;

import be.bf.init.demo.entities.Article;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ArticleService {
    List<Article> findAll();
    Article insert(Article toInsert);
}
