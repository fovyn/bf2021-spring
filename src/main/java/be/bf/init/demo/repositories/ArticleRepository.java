package be.bf.init.demo.repositories;

import be.bf.init.demo.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    @Query(value = "SELECT a FROM Article a WHERE a.title = :str")
    List<Article> findAllByTitle(@Param(value = "str") String str);

    List<Article> findAllByTitleStartingWith(String str);
    List<Article> findAllByTitleAndAndPublishDate(String title, LocalDate publishDate);
}
