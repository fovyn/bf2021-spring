package be.bf.init.demo.controllers;

import be.bf.init.demo.entities.Article;
import be.bf.init.demo.models.macros.DataTableModel;
import be.bf.init.demo.services.ArticleService;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller()
public class DemoController {
    private final ArticleService service;
    private final Logger logger = LoggerFactory.getLogger(DemoController.class);

    @Autowired
    public DemoController(ArticleService service) {
        this.service = service;
    }

    @GetMapping(path = {"", "/", "/demo", "/home"})
    public String homeAction(Model view) {
        List<Article> articles = service.findAll();

        view.addAttribute("title", "Hello world !!");
        DataTableModel<Article, String, Object> model = new DataTableModel<>();

//        Table<Article, String, Object> table = HashBasedTable.create();
        for (Article a : articles) {
            model.put(a, "Id", a.getId());
            model.put(a, "Title", a.getTitle());
            model.put(a, "PublishDate", a.getPublishDate());
        }

        view.addAttribute("table", model);
        return "demo/home";
    }
}
