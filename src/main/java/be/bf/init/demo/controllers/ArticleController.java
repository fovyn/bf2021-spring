package be.bf.init.demo.controllers;

import be.bf.init.demo.entities.Article;
import be.bf.init.demo.models.forms.ArticleForm;
import be.bf.init.demo.services.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping(path = {"/articles"})
public class ArticleController {
    private Logger logger = LoggerFactory.getLogger(ArticleController.class);
    private final ArticleService service;

    public ArticleController(ArticleService service) {
        this.service = service;
    }

    @GetMapping(name = "ArticleCreate",path = {"/create"})
    public String createAction() {
        return "article/create";
    }

    @PostMapping(path = {"/create"})
    public String createAction(@Valid ArticleForm form, BindingResult validation) {
        if (validation.hasErrors()) {
            logger.warn("ERROR");
            return "redirect:/articles/create";
        }
        logger.warn("ERRORS => "+ validation.getAllErrors());
//        logger.warn("Title = "+ title);
//        logger.warn("PublishDate = "+ publishDate);
        logger.warn("Form = "+ form);
        Article toInsert = new Article();
        toInsert.setTitle(form.getTitle());
        toInsert.setPublishDate(LocalDate.parse(form.getPublishDate()));
        this.service.insert(toInsert);
        return "redirect:/articles/create";
    }
}
