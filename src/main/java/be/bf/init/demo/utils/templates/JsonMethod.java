package be.bf.init.demo.utils.templates;

import com.google.gson.Gson;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

import java.util.List;

public class JsonMethod implements TemplateMethodModelEx {
    @Override
    public TemplateModel exec(List list) throws TemplateModelException {
        return new SimpleScalar(new Gson().toJson(list.get(0)));
    }
}
